package com.d4z.request.handlers;

import com.d4z.request.interfaces.IAbstractGetHandler;

import io.vertx.core.json.JsonObject;

public class CommentGetHandler implements IAbstractGetHandler<JsonObject> {
	@Override
	public JsonObject get() {
		return new JsonObject().put("message", "CommentGetHandler");
	}
}
