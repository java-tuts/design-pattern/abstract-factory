package com.d4z.request.handlers;


import com.d4z.request.interfaces.IAbstractPostHandler;

import io.vertx.core.json.JsonObject;

public class CommentPostHandler implements IAbstractPostHandler<JsonObject> {
	@Override
	public JsonObject post() {
		return new JsonObject().put("message", "CommentPostHandler");
	}
}