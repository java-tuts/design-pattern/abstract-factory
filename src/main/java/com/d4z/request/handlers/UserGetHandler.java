package com.d4z.request.handlers;

import com.d4z.request.interfaces.IAbstractGetHandler;

import io.vertx.core.json.JsonObject;

public class UserGetHandler implements IAbstractGetHandler<JsonObject> {
	@Override
	public JsonObject get() {
		// TODO Auto-generated method stub
		return new JsonObject().put("message", "UserGetHandler");
	}
}