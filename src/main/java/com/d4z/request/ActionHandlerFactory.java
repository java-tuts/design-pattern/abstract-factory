package com.d4z.request;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import com.d4z.request.factories.IAbstractFactory;

import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;

public enum ActionHandlerFactory {
	INSTANCE;
	
	private Map<String, String> factoryMapping;
	
	ActionHandlerFactory() {
		factoryMapping = new HashMap<String, String>();
		factoryMapping.put("user", "com.d4z.request.factories.ApiUserFactory");
		factoryMapping.put("comment", "com.d4z.request.factories.ApiCommentFactory");
	}
	
	private IAbstractFactory<JsonObject> getFactory(String pAction) throws Exception {
		final String classHandler = factoryMapping.get(pAction);
		if(classHandler != null) {
			Class<?> myClass = Class.forName(classHandler);
			Constructor<?> constructor = myClass.getConstructor();
			IAbstractFactory<JsonObject> actionHandler = (IAbstractFactory) constructor.newInstance();
			return actionHandler;
		}
		else {
			throw new UnsupportedOperationException("ActionFactoryNotFound");
		}
	}
	
	public JsonObject doAction(String pAction, HttpMethod pMethod) throws Exception {
		IAbstractFactory<JsonObject> lvFactory = this.getFactory(pAction);
		switch (pMethod.name()) {
			case "POST":
				return lvFactory.createPostHandler().post();
			case "GET":
				return lvFactory.createGetHandler().get();
			default:
				throw new UnsupportedOperationException("Method not found!");
		}
	}
}
