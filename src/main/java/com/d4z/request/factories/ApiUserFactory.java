package com.d4z.request.factories;

import com.d4z.request.handlers.UserGetHandler;
import com.d4z.request.handlers.UserPostHandler;
import com.d4z.request.interfaces.IAbstractGetHandler;
import com.d4z.request.interfaces.IAbstractPostHandler;

import io.vertx.core.json.JsonObject;

public class ApiUserFactory implements IAbstractFactory<JsonObject> {
	@Override
	public IAbstractGetHandler<JsonObject> createGetHandler() {
		return new UserGetHandler();
	}
	
	@Override
	public IAbstractPostHandler<JsonObject> createPostHandler() {
		return new UserPostHandler();
	}
}
