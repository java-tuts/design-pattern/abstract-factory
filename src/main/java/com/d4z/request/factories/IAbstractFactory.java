package com.d4z.request.factories;

import com.d4z.request.interfaces.IAbstractGetHandler;
import com.d4z.request.interfaces.IAbstractPostHandler;

public interface IAbstractFactory<T> {
	public IAbstractPostHandler<T> createPostHandler();
	public IAbstractGetHandler<T> createGetHandler();
}
