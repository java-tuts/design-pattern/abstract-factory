package com.d4z.request.factories;

import com.d4z.request.handlers.CommentGetHandler;
import com.d4z.request.handlers.CommentPostHandler;
import com.d4z.request.interfaces.IAbstractGetHandler;
import com.d4z.request.interfaces.IAbstractPostHandler;

import io.vertx.core.json.JsonObject;

public class ApiCommentFactory implements IAbstractFactory<JsonObject> {
	@Override
	public IAbstractGetHandler<JsonObject> createGetHandler() {
		return new CommentGetHandler();
	}
	
	@Override
	public IAbstractPostHandler<JsonObject> createPostHandler() {
		return new CommentPostHandler();
	}
}
