package com.d4z.request.interfaces;

public interface IAbstractDeleteHandler<T> {
	public T delete();
}
