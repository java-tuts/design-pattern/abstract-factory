package com.d4z.request.interfaces;

public interface IAbstractGetHandler<T> {
	public T get();
	
}
