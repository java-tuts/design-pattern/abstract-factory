package com.d4z;

import io.vertx.core.Launcher;

public class RunMain extends Launcher {
	static String DEFAULT_CFG_PATH = "src/main/resources/config/config.json";
	static final String DCONFIGPATH = "configPath";

	public static void main(String[] args) {
		new RunMain().dispatch(args);
	}
}
