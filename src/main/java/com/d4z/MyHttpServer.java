package com.d4z;

import com.d4z.request.ActionHandlerFactory;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;


public class MyHttpServer extends AbstractVerticle {

	@Override
	public void start() throws Exception {
		super.start();
		HttpServerOptions httpOption = new HttpServerOptions();
		Router router = Router.router(this.vertx);

		router.route("/api/:action").handler(BodyHandler.create()).handler(rtx -> {
			final String action = rtx.pathParam("action");
			JsonObject response;
			int statusCode = 200;
			
			try {
				response = ActionHandlerFactory.INSTANCE.doAction(action, rtx.request().method());
			} catch (Exception e) {
				e.printStackTrace();
				response = new JsonObject().put("errors", e.getMessage());
				statusCode = 500;
			}
			
			rtx.response()
			 .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
			 .setStatusCode(statusCode)
			 .end(response.toString());
		});

		HttpServer server = vertx.createHttpServer(httpOption);
		server.requestHandler(router).listen(33000, res -> {
			if (res.succeeded()) {
				System.out.println("Started http://localhost:33000/api/");
			} else {
				res.cause().printStackTrace();
			}
		});
	}
}
